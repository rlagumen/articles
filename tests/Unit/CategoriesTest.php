<?php

namespace Unit;

use App\Models\Category;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Collection;
use TestCase;

class CategoriesTest extends TestCase
{
    protected function setUp(): void
    {
        parent::setUp(); // TODO: Change the autogenerated stub

        $this->category = new Category();
    }

    /** @test */
    public function a_category_has_many_articles()
    {
        $this->assertTrue($this->category->articles() instanceof HasMany);
        $this->assertTrue($this->category->articles instanceof Collection);
    }

    /** @test */
    public function it_belongs_to_a_creator()
    {
        $this->assertInstanceOf(BelongsTo::class, $this->category->creator());
    }
}
