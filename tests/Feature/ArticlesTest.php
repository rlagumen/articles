<?php

namespace Feature;

use App\Models\Article;
use App\Models\Category;
use App\Models\User;
use Illuminate\Support\Str;
use Laravel\Lumen\Testing\DatabaseMigrations;
use TestCase;
use function route;

class ArticlesTest extends TestCase
{
    use DatabaseMigrations;

    protected function setUp(): void
    {
        parent::setUp();
    }

    /** @test */
    public function can_view_list_of_articles()
    {
        $user = User::factory()->create();

        $category = Category::factory()->state(['creator_id' => $user->id])->create();
        Article::factory()->count(10)->state([
            'creator_id' => $user->id,
            'status' => Article::approved(),
            'category_id' => $category->id
        ])->create();

        $response = $this->call('GET', route('articles.index'));

        $response->assertJsonCount(10, 'data')
            ->assertStatus(200);
    }

    /** @test */
    public function can_view_specific_article()
    {
        $user = User::factory()->create();

        $category = Category::factory()->state(['creator_id' => $user->id])->create();
        $article = Article::factory()->state(['creator_id' => $user->id, 'category_id' => $category->id])->create();

        $response = $this->call('GET', route('articles.show', ['slug' => $article->slug]));

        $response->assertJsonFragment([
            'slug' => $article->slug,
            'title' => $article->title,
            'status' => $article->fresh()->status,
            'content' => $article->content
        ])->assertStatus(200);
    }
}
