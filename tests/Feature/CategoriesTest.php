<?php

namespace Feature;

use App\Models\Article;
use App\Models\Category;
use App\Models\User;
use Illuminate\Support\Str;
use Laravel\Lumen\Testing\DatabaseMigrations;
use TestCase;
use function route;

class CategoriesTest extends TestCase
{
    use DatabaseMigrations;

    protected function setUp(): void
    {
        parent::setUp();
    }

    /** @test */
    public function can_view_list_of_categories()
    {
        Category::factory()->count(10)->create();

        $response = $this->call('GET', route('categories.index'));

        $response->assertJsonCount(10, 'data')
            ->assertStatus(200);
    }

    /** @test */
    public function can_view_specific_category()
    {
        $category = Category::factory()->create();

        $response = $this->call('GET', route('categories.show', ['slug' => $category->slug]));

        $response->assertJsonFragment([
            'slug' => $category->slug,
            'name' => $category->name
        ])->assertStatus(200);
    }
}
