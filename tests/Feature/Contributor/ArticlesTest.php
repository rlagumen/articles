<?php

namespace Feature\Contributor;

use App\Models\Article;
use App\Models\Category;
use App\Models\User;
use Illuminate\Support\Str;
use Laravel\Lumen\Testing\DatabaseMigrations;
use TestCase;
use function route;

class ArticlesTest extends TestCase
{
    use DatabaseMigrations;

    protected function setUp(): void
    {
        parent::setUp();

        $this->user = User::factory()
            ->state(['role' => User::contributor()])
            ->create();

        $this->actingAs($this->user);
    }

    /** @test */
    public function can_view_list_of_articles()
    {
        Article::factory()->count(10)->create();

        $response = $this->call('GET', route('contributor.articles.index'));

        $response->assertJsonCount(10, 'data')
            ->assertStatus(200);
    }

    /** @test */
    public function can_view_specific_article()
    {
        $article = Article::factory()->create();

        $response = $this->call('GET', route('contributor.articles.show', ['id' => $article->id]));

        $response->assertJsonFragment([
            'id' => $article->id,
            'slug' => $article->slug,
            'title' => $article->title,
            'status' => $article->fresh()->status,
            'content' => $article->content
        ])->assertStatus(200);
    }

    /** @test */
    public function cannot_view_specific_article_that_is_not_owned_by_auth_user()
    {
        $contributor1 = User::factory()->state(['role' => User::contributor()])->create();
        $this->actingAs($contributor1);
        $article1 = Article::factory()->create();

        $contributor2 = User::factory()->state(['role' => User::contributor()])->create();
        $this->actingAs($contributor2);

        $response = $this->call('GET', route('contributor.articles.show', ['id' => $article1->id]));

        $response->assertStatus(403);
    }

    /** @test */
    public function can_create_article()
    {
        $category = Category::factory()->create();

        $response = $this->call('POST', route('contributor.articles.store'), [
            'category' => $category->id,
            'title' => $title = 'An interesting article about space',
            'content' => $content = 'This is an interesting article about space',
            'excerpt' => $excerpt = 'A short description about this article',
        ]);

        $response->assertJsonFragment([
            'title' => $title,
            'content' => $content,
            'excerpt' => $excerpt,
        ])->assertStatus(201);

        $this->seeInDatabase('articles', [
            'creator_id' => $this->user->id,
            'category_id' => $category->id,
            'title' => $title,
            'slug' => Str::slug($title),
            'content' => $content,
            'excerpt' => $excerpt
        ]);
    }

    /** @test */
    public function cannot_create_article_with_invalid_data()
    {
        $category = Category::factory()->create();

        $response = $this->call('POST', route('contributor.articles.store'), [
            'category' => $category->id,
            'content' => $content = 'This is an interesting article about space',
            'excerpt' => $excerpt = 'A short description about this article',
        ]);

        $response->assertStatus(422);

        $this->missingFromDatabase('articles', [
            'category_id' => $category->id,
            'content' => $content,
            'excerpt' => $excerpt
        ]);
    }

    /** @test */
    public function can_update_article()
    {
        $article = Article::factory()->create();

        $response = $this->call('PATCH', route('contributor.articles.update', ['id' => $article->id]), [
            'title' => $title = 'Updated Article Title',
        ]);

        $response->assertJsonFragment([
            'id' => $article->id,
            'slug' => $article->fresh()->slug,
            'title' => $title,
            'content' => $article->content
        ]);

        $this->seeInDatabase('articles', [
            'id' => $article->id,
            'title' => $title,
            'slug' => Str::slug($title)
        ]);
    }

    /** @test */
    public function can_auto_generate_unique_slug_when_existing()
    {
        $article1 = Article::factory()->create();
        $article2 = Article::factory()->create();

        $response = $this->call('PATCH', route('contributor.articles.update', ['id' => $article2->id]), [
            'title' => $article1->title,
        ]);

        $response->assertJsonFragment([
            'id' => $article2->id,
            'slug' => Str::slug($article2->fresh()->title) . "-" . $article2->id,
            'title' => $article1->title,
            'content' => $article2->content
        ]);

        $this->seeInDatabase('articles', [
            'id' => $article2->id,
            'title' => $article1->title,
            'slug' => Str::slug($article2->fresh()->title) . "-" . $article2->id
        ]);
    }

    /** @test */
    public function can_delete_article()
    {
        $article = Article::factory()->create();

        $response = $this->call('DELETE', route('contributor.articles.destroy', ['id' => $article->id]));

        $response->assertStatus(204);

        $this->assertNotNull($article->fresh()->deleted_at);
    }
}
