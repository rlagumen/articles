<?php

namespace Feature\Contributor;

use App\Models\Article;
use App\Models\Category;
use App\Models\User;
use Illuminate\Support\Str;
use Laravel\Lumen\Testing\DatabaseMigrations;
use TestCase;
use function route;

class CategoriesTest extends TestCase
{
    use DatabaseMigrations;

    protected function setUp(): void
    {
        parent::setUp();

        $this->user = User::factory()
            ->state(['role' => User::contributor()])
            ->create();

        $this->actingAs($this->user);
    }

    /** @test */
    public function can_view_list_of_categories()
    {
        Category::factory()->count(10)->create();

        $response = $this->call('GET', route('contributor.categories.index'));

        $response->assertJsonCount(10, 'data')
            ->assertStatus(200);
    }
}
