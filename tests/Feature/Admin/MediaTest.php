<?php

namespace Feature\Admin;

use App\Models\Category;
use App\Models\User;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Laravel\Lumen\Testing\DatabaseMigrations;
use TestCase;
use function route;

class MediaTest extends TestCase
{
    use DatabaseMigrations;

    protected function setUp(): void
    {
        parent::setUp();

        Storage::fake();

        $this->user = User::factory()
            ->state(['role' => User::admin()])
            ->create();

        $this->actingAs($this->user);
    }

    /** @test */
    public function can_create_article_with_media()
    {
        $category = Category::factory()->create();

        $response = $this->call('POST', route('admin.articles.store'), [
            'category' => $category->id,
            'title' => $title = 'An interesting article about space',
            'featured_image' => $file = UploadedFile::fake()->image('featured-image.png')
        ]);

        $response->assertJsonFragment([
            'title' => $title,
            'filename' => $file->hashName()
        ])->assertStatus(201);

        $this->seeInDatabase('articles', [
            'creator_id' => $this->user->id,
            'category_id' => $category->id,
            'title' => $title,
            'slug' => Str::slug($title)
        ]);

        $this->seeInDatabase('media', [
            'model_type' => 'article',
            'collection_name' => 'featured_image',
            'filename' => $file->hashName(),
            'mime' => $file->getMimeType()
        ]);
    }
}
