<?php

namespace Feature\Admin;

use App\Models\Category;
use App\Models\User;
use Illuminate\Support\Str;
use Laravel\Lumen\Testing\DatabaseMigrations;
use TestCase;
use function route;

class CategoriesTest extends TestCase
{
    use DatabaseMigrations;

    protected function setUp(): void
    {
        parent::setUp();

        $this->user = User::factory()
            ->state(['role' => User::admin()])
            ->create();

        $this->actingAs($this->user);
    }

    /** @test */
    public function can_view_list_of_categories()
    {
        Category::factory()->count(10)->create();

        $response = $this->call('GET', route('admin.categories.index'));

        $response->assertJsonCount(10, "data")
            ->assertStatus(200);
    }

    /** @test */
    public function can_view_specific_category()
    {
        $category = Category::factory()->create();

        $response = $this->call('GET', route('admin.categories.show', ['id' => $category->id]));

        $response->assertJsonFragment([
            'id' => $category->id,
            'slug' => $category->slug,
            'name' => $category->name
        ])->assertStatus(200);
    }

    /** @test */
    public function can_create_category()
    {
        $response = $this->call('POST', route('admin.categories.store'), [
            'name' => $name = 'Sci-Fi',
        ]);

        $response->assertJsonFragment([
            'slug' => Str::slug($name),
            'name' => $name
        ])->assertStatus(201);

        $this->seeInDatabase('categories', [
            'creator_id' => $this->user->id,
            'name' => $name,
            'slug' => Str::slug($name)
        ]);
    }

    /** @test */
    public function cannot_create_category_with_invalid_data()
    {
        $response = $this->call('POST', route('admin.categories.store'));

        $response->assertStatus(422);
    }

    /** @test */
    public function can_update_category()
    {
        $category = Category::factory()->create();

        $response = $this->call('PATCH', route('admin.categories.update', ['id' => $category->id]), [
            'name' => $name = 'Updated Category Name',
        ]);

        $response->assertJsonFragment([
            'slug' => Str::slug($name),
            'name' => $name
        ])->assertStatus(200);

        $this->seeInDatabase('categories', [
            'id' => $category->id,
            'creator_id' => $this->user->id,
            'name' => $name,
        ]);
    }

    /** @test */
    public function can_delete_category()
    {
        $category = Category::factory()->create();

        $response = $this->call('DELETE', route('admin.categories.destroy', ['id' => $category->id]));

        $response->assertStatus(204);

        $this->missingFromDatabase('categories', [
            'id' => $category->id
        ]);
    }
}
