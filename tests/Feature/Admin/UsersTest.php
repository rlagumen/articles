<?php

namespace Feature\Admin;

use App\Models\User;
use Illuminate\Hashing\BcryptHasher;
use Laravel\Lumen\Testing\DatabaseMigrations;
use TestCase;
use function route;

class UsersTest extends TestCase
{
    use DatabaseMigrations;

    protected function setUp(): void
    {
        parent::setUp();

        $this->user = User::factory()
            ->state(['role' => User::admin()])
            ->create();

        $this->actingAs($this->user);
    }

    /** @test */
    public function cannot_access_users_list_if_auth_user_is_not_admin()
    {
        $this->user = User::factory()
            ->state(['role' => User::contributor()])
            ->create();

        $this->actingAs($this->user);

        $response = $this->call('GET', route('admin.users.index'));

        $response->assertStatus(403);
    }

    /** @test */
    public function can_view_list_of_users()
    {
        User::factory()->count(10)->create();

        $response = $this->call('GET', route('admin.users.index'));

        $response->assertJsonCount(11, 'data')
            ->assertStatus(200);
    }

    /** @test */
    public function can_view_specific_user()
    {
        $user = User::factory()->create();

        $response = $this->call('GET', route('admin.users.show', ['id' => $user->id]));

        $response->assertJsonFragment([
            'id' => $user->id,
            'full_name' => $user->full_name,
            'first_name' => $user->first_name,
            'last_name' => $user->last_name,
            'email' => $user->email,
            'role' => $user->role,
        ])->assertStatus(200);
    }

    /** @test */
    public function can_create_user()
    {
        $response = $this->call('POST', route('admin.users.store'), [
            'first_name' => $firstName = 'Ralph',
            'last_name' => $lastName = 'Lagumen',
            'email' => $email = 'ralph.lagumen@optimy.com',
            'role' => $role = User::contributor(),
            'password' => $password = 'secret',
            'password_confirmation' => $password
        ]);

        $response->assertJsonFragment([
            'first_name' => $firstName,
            'last_name' => $lastName,
            'email' => $email,
            'role' => $role,
        ])->assertStatus(201);

        $this->seeInDatabase('users', [
            'first_name' => $firstName,
            'last_name' => $lastName,
            'email' => $email,
            'role' => $role,
        ]);

        $user = User::where(['email' => $email])->first();

        $this->assertTrue((new BcryptHasher())->check($password, $user->password));
    }

    /** @test */
    public function cannot_create_user_with_invalid_data()
    {
        $response = $this->call('POST', route('admin.users.store'), [
            'first_name' => $firstName = 'Ralph',
            'email' => $email = 'notanemail',
            'role' => $role = User::contributor(),
            'password' => $password = 'secret',
            'password_confirmation' => $password
        ]);

        $response->assertStatus(422);

        $this->missingFromDatabase('users', [
            'first_name' => $firstName,
            'email' => $email,
            'role' => $role,
        ]);
    }

    /** @test */
    public function can_update_user()
    {
        $user = User::factory()->create();

        $response = $this->call('PATCH', route('admin.users.update', ['id' => $user->id]), [
            'first_name' => $firstName = 'John',
            'last_name' => $lastName = 'Doe',
            'role' => $role = User::admin(),
        ]);

        $response->assertJsonFragment([
            'id' => $user->id,
            'first_name' => $firstName,
            'last_name' => $lastName,
            'role' => $role,
        ])->assertStatus(200);

        $this->seeInDatabase('users', [
            'id' => $user->id,
            'first_name' => $firstName,
            'last_name' => $lastName,
            'role' => $role
        ]);
    }

    /** @test */
    public function can_delete_user()
    {
        $user = User::factory()->create();

        $response = $this->call('DELETE', route('admin.users.destroy', ['id' => $user->id]));

        $response->assertStatus(204);

        $this->assertNotNull($user->fresh()->deleted_at);
    }

    /** @test */
    public function can_restore_user()
    {
        $user = User::factory()->create();

        $response = $this->call('PATCH', route('admin.users.restore', ['id' => $user->id]));

        $response->assertStatus(204);

        $this->assertNull($user->fresh()->deleted_at);
    }
}
