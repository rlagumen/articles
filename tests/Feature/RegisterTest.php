<?php

namespace Feature;

use Illuminate\Support\Facades\Auth;
use Laravel\Lumen\Testing\DatabaseMigrations;
use TestCase;

class RegisterTest extends TestCase
{
    use DatabaseMigrations;

    protected function setUp(): void
    {
        parent::setUp();
    }

    /** @test */
    public function can_visit_register_page()
    {
        $response = $this->call('GET', '/register');

        $response->assertViewIs('auth.register')
            ->assertStatus(200);
    }

    /** @test */
    public function can_register_and_login_automatically()
    {
        $response = $this->call('POST', '/register', [
            'first_name' => $firstName = 'Ralph',
            'last_name' => $lastName = 'Lagumen',
            'email' => $email = 'ralph.lagumen@optimy.com',
            'password' => $password = 'secret',
            'password_confirmation' => $password
        ]);

        $response->assertRedirect(route('contributor.articles.index'))
            ->assertStatus(302);

        $this->seeInDatabase('users', [
            'first_name' => $firstName,
            'last_name' => $lastName,
            'email' => $email
        ]);

        $this->assertEquals(Auth::user()->email, $email);
    }
}
