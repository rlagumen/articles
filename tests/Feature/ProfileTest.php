<?php

namespace Feature;

use App\Models\User;
use Laravel\Lumen\Testing\DatabaseMigrations;
use TestCase;

class ProfileTest extends TestCase
{
    use DatabaseMigrations;

    protected function setUp(): void
    {
        parent::setUp();
    }

    /** @test */
    public function can_visit_profile_page()
    {
        $user = User::factory()->create();
        $this->actingAs($user);

        $response = $this->call('GET', route('profile.index'));

        $response->assertViewIs('auth.profile')
            ->assertViewHas('user', $user)
            ->assertStatus(200);
    }

    /** @test */
    public function can_update_profile()
    {
        $user = User::factory()->create();
        $this->actingAs($user);

        $response = $this->call('PATCH', route('profile.update'), [
            'first_name' => $firstName = 'Ralph',
            'last_name' => $lastName = 'Lagumen',
            'email' => $email = 'ralph.lagumen@optimy.com'
        ]);

        $response->assertRedirect(route('profile.index'))
            ->assertStatus(302);

        $this->seeInDatabase('users', [
            'first_name' => $firstName,
            'last_name' => $lastName,
            'email' => $email
        ]);
    }

    /** @test */
    public function cannot_update_profile_if_unauthenticated()
    {
        $response = $this->call('PATCH', route('profile.update'), [
            'first_name' => 'Ralph',
            'last_name' => 'Lagumen',
            'email' => 'ralph.lagumen@optimy.com'
        ]);

        $response->assertStatus(401);
    }
}
