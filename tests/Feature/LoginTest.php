<?php

namespace Feature;

use App\Models\User;
use Illuminate\Hashing\BcryptHasher;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Laravel\Lumen\Testing\DatabaseMigrations;
use TestCase;

class LoginTest extends TestCase
{
    use DatabaseMigrations;

    protected function setUp(): void
    {
        parent::setUp();
    }

    /** @test */
    public function can_login_with_correct_credentials()
    {
        $user = User::factory()->state([
            'role' => User::admin(),
            'email' => $email = 'ralph.lagumen@optimy.com',
            'password' => (new BcryptHasher())->make($password = 'secret')
        ])->create();

        $response = $this->call('POST', '/spa/login', [
            'email' => $email,
            'password' => $password
        ]);

        $this->call('GET', route('admin.articles.index'), [], ['HTTP_Authorization' => 'asd']);

        $this->assertEquals(Auth::id(), $user->id);
    }
}
