<?php

namespace App\Providers;

use App\Models\User;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * List of available policies
     *
     * @var string[]
     */
    protected $policies = [
        'categories' => 'App\Policies\CategoriesPolicy',
        'articles' => 'App\Policies\ArticlesPolicy',
        'users' => 'App\Policies\UsersPolicy'
    ];

    /**
     * Boot the authentication services for the application.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        // Here you may define how you wish users to be authenticated for your Lumen
        // application. The callback which receives the incoming request instance
        // should return either a User instance or null. You're free to obtain
        // the User instance via an API token or any other method necessary.
        $this->app['auth']->viaRequest('api', function ($request) {
            if ($request->input('api_token')) {
                return User::where('api_token', $request->input('api_token'))->first();
            }
        });
    }

    /**
     * Registering policies
     *
     * @return void
     */
    public function registerPolicies()
    {
        foreach ($this->policies as $policy => $handler) {
            foreach ($this->abilities() as $ability) {
                Gate::define($policy.'.'.$ability, $handler.'@'.$ability);
            }
        }
    }

    /**
     * List of available abilities
     *
     * @return string[]
     */
    public function abilities()
    {
        return [
            'destroy',
            'restore',
            'update',
            'index',
            'store',
            'show',
        ];
    }
}
