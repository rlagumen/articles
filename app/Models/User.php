<?php

namespace App\Models;

use App\Models\Traits\HasRoles;
use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Laravel\Lumen\Auth\Authorizable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Model implements AuthenticatableContract, AuthorizableContract, JWTSubject
{
    use HasRoles,
        HasFactory,
        Authenticatable,
        Authorizable,
        SoftDeletes;

    protected $guarded = ['id'];

    const ROLES = [
        'admin',
        'contributor'
    ];

    public function getFullNameAttribute()
    {
        return "{$this->first_name} {$this->last_name}";
    }

    /**
     * Relationships
     */
    public function articles()
    {
        return $this->hasMany(Article::class, 'creator_id');
    }

    public function categories()
    {
        return $this->hasMany(Category::class, 'creator_id');
    }

    public function token()
    {
        return $this->hasOne(Token::class, 'user_id');
    }

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }
}
