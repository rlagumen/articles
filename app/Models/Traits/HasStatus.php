<?php

namespace App\Models\Traits;

use App\Models\Article;

trait HasStatus
{
    public static function draft()
    {
        return Article::STATUS[0];
    }

    public static function pending()
    {
        return Article::STATUS[1];
    }

    public static function approved()
    {
        return Article::STATUS[2];
    }
}
