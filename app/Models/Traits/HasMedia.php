<?php

namespace App\Models\Traits;

use App\Models\Media;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

trait HasMedia
{
    public function media()
    {
        return $this->morphOne(Media::class, 'model');
    }

    public function addMedia(UploadedFile $file, $collectionName = 'attachment')
    {
        $this->media()->delete();

        $this->media()->create([
            'collection_name' => $collectionName,
            'filename' => $file->hashName(),
            'mime' => $file->getMimeType(),
            'path' => Storage::putFileAs(
                'media', $file, $file->hashName()
            )
        ]);
    }
}
