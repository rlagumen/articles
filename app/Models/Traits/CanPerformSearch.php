<?php

namespace App\Models\Traits;

use App\Services\SearchableModel;

trait CanPerformSearch
{
    public function scopeSearch($query, $value)
    {
        if (empty($this->searchableColumns)) {
            return $this;
        }

        return $query->where(function ($builder) use ($value) {
            foreach ($this->searchableColumns as $key => $column) {
                if (is_array($column)) {
                    $builder->orWhereHas($key, function ($builder) use ($column, $value) {
                        foreach ($column as $relationColumn) {
                            $builder->where($relationColumn, 'LIKE', '%'.$value.'%');
                        }
                    });
                } else {
                    $builder->orWhere($builder->qualifyColumn($column), 'LIKE', '%'.$value.'%');
                }
            }
        });
    }

    public function scopeFilter($query, $value = [])
    {
        return app(SearchableModel::class)
            ->builder($query)
            ->filter($value);
    }

    public function scopeEloquentRaw($query, $value = [])
    {
        return app(SearchableModel::class)
            ->raw()
            ->builder($query)
            ->filter($value);
    }
}
