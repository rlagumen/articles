<?php

namespace App\Models\Traits;

use App\Models\User;

trait HasRoles
{
    public static function admin()
    {
        return User::ROLES[0];
    }

    public static function contributor()
    {
        return User::ROLES[1];
    }

    public function isAdmin()
    {
        return $this->role === User::admin();
    }

    public function isContributor()
    {
        return $this->role === User::contributor();
    }
}
