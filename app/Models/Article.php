<?php

namespace App\Models;

use App\Models\Traits\CanPerformSearch;
use App\Models\Traits\HasMedia;
use App\Models\Traits\HasStatus;
use App\Observers\ArticleObserver;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class Article extends Model
{
    use CanPerformSearch,
        ArticleObserver,
        SoftDeletes,
        HasFactory,
        HasStatus,
        HasMedia;

    protected $guarded = ['id'];

    protected $casts = ['created_at' => 'datetime:Y-m-d'];

    protected $searchableColumns = [
        'id',
        'slug',
        'title',
        'status'
    ];

    const STATUS = [
        'draft',
        'pending',
        'approved'
    ];

    public function generateSlug($title)
    {
        $slug = Str::slug($title);

        return $this->where('slug', $slug)->exists()
            ? $slug."-".$this->id
            : $slug;
    }

    public function isOwned()
    {
        return $this->where('creator_id', Auth::id())->exists();
    }

    /**
     * Relationships
     */
    public function creator()
    {
        return $this->belongsTo(User::class, 'creator_id');
    }

    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }
}
