<?php

namespace App\Models;

use App\Observers\MediaObserver;
use Illuminate\Database\Eloquent\Model;

class Media extends Model
{
    use MediaObserver;

    protected $guarded = ['id'];

    protected $table = 'media';
}
