<?php

namespace App\Models;

use App\Observers\CategoryObserver;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use CategoryObserver,
        HasFactory;

    protected $guarded = ['id'];

    /**
     * Relationship
     */
    public function articles()
    {
        return $this->hasMany(Article::class, 'category_id');
    }

    public function creator()
    {
        return $this->belongsTo(User::class, 'creator_id');
    }
}
