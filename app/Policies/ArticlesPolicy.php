<?php

namespace App\Policies;

use App\Models\Article;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ArticlesPolicy
{
    use HandlesAuthorization;

    public function show(User $user, Article $article)
    {
        if ($user->isAdmin()) {
            return true;
        }

        if ($user->isContributor() && $article->isOwned()) {
            return true;
        }

        return false;
    }

    public function store(User $user)
    {
        if ($user->isAdmin() || $user->isContributor()) {
            return true;
        }

        return false;
    }

    public function update(User $user)
    {
        if ($user->isAdmin() || $user->isContributor()) {
            return true;
        }

        return false;
    }

    public function destroy(User $user)
    {
        if ($user->isAdmin() || $user->isContributor()) {
            return true;
        }

        return false;
    }

    public function restore(User $user)
    {
        if ($user->isAdmin()) {
            return true;
        }

        return false;
    }
}
