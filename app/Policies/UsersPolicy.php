<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UsersPolicy
{
    use HandlesAuthorization;

    public function index(User $user)
    {
        if ($user->isAdmin()) {
            return true;
        }

        return false;
    }

    public function show(User $user)
    {
        if ($user->isAdmin()) {
            return true;
        }

        return false;
    }

    public function store(User $user)
    {
        if ($user->isAdmin()) {
            return true;
        }

        return false;
    }

    public function update(User $user)
    {
        if ($user->isAdmin()) {
            return true;
        }

        return false;
    }

    public function destroy(User $user)
    {
        if ($user->isAdmin()) {
            return true;
        }

        return false;
    }

    public function restore(User $user)
    {
        if ($user->isAdmin()) {
            return true;
        }

        return false;
    }
}
