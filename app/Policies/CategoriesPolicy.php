<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class CategoriesPolicy
{
    use HandlesAuthorization;

    public function store(User $user)
    {
        if ($user->isAdmin() || $user->isContributor()) {
            return true;
        }

        return false;
    }

    public function update(User $user)
    {
        if ($user->isAdmin() || $user->isContributor()) {
            return true;
        }

        return false;
    }

    public function destroy(User $user)
    {
        if ($user->isAdmin() || $user->isContributor()) {
            return true;
        }

        return false;
    }
}
