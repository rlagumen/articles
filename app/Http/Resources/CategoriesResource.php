<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CategoriesResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'slug' => $this->slug,
            'name' => $this->name,
            'articles' => $this->includeArticles(),
            'created_at' => $this->created_at
        ];
    }

    public function includeArticles()
    {
        if ($this->relationLoaded('articles')) {
            return ArticlesResources::collection($this->articles);
        }

        return null;
    }
}
