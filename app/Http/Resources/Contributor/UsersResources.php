<?php

namespace App\Http\Resources\Contributor;

use Illuminate\Http\Resources\Json\JsonResource;

class UsersResources extends JsonResource
{
    public function toArray($request)
    {
        return [
            'full_name' => $this->full_name,
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'role' => $this->role,
            'email' => $this->email,
            'created_at' => $this->created_at
        ];
    }
}
