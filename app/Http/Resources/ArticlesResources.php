<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ArticlesResources extends JsonResource
{
    public function toArray($request)
    {
        return [
            'slug' => $this->slug,
            'creator' => $this->includeCreator(),
            'category' => $this->includeCategory(),
            'featured_image' => $this->includeMedia(),
            'title' => $this->title,
            'status' => $this->status,
            'content' => $this->content,
            'excerpt' => $this->excerpt,
            'created_at' => $this->created_at
        ];
    }

    public function includeCreator()
    {
        if ($this->relationLoaded('creator')) {
            return new UsersResources($this->creator);
        }

        return null;
    }

    public function includeCategory()
    {
        if ($this->relationLoaded('category')) {
            return new CategoriesResource($this->category);
        }

        return null;
    }

    public function includeMedia()
    {
        if ($this->relationLoaded('media')) {
            return $this->media;
        }

        return null;
    }
}
