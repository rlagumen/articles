<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UsersResources extends JsonResource
{
    public function toArray($request)
    {
        return [
            'full_name' => $this->full_name,
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'role' => $this->role,
            'email' => $this->email,
            'created_at' => $this->created_at,
            'token' => $this->includeToken()
        ];
    }

    public function includeToken()
    {
        if ($this->relationLoaded('token')) {
            return $this->token->payload;
        }

        return null;
    }
}
