<?php

namespace App\Http\Validations;

class ArticleValidation
{
    public function save()
    {
        return [
            'category' => 'required|exists:categories,id',
            'title' => 'required|string|max:50',
            'content' => 'nullable|string',
            'excerpt' => 'nullable|string|max:150',
            'featured_image' => 'nullable|image|max:2048'
        ];
    }

    public function update()
    {
        return [
            'category' => 'sometimes|exists:categories,id',
            'title' => 'sometimes|string|max:50',
            'content' => 'nullable|string',
            'excerpt' => 'nullable|string|max:150',
            'featured_image' => 'nullable|image|max:2048'
        ];
    }
}
