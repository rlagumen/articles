<?php

namespace App\Http\Validations;

use App\Models\User;
use Illuminate\Validation\Rule;

class UserValidation
{
    public function save()
    {
        return [
            'first_name' => 'required|string',
            'last_name' => 'required|string',
            'role' => [
                'sometimes',
                'string',
                Rule::in(User::ROLES)
            ],
            'email' => 'required|email|unique:users,email',
            'password' => 'required|string|confirmed'
        ];
    }

    public function update()
    {
        return [
            'first_name' => 'sometimes|string',
            'last_name' => 'sometimes|string',
            'role' => [
                'sometimes',
                'string',
                Rule::in(User::ROLES)
            ],
            'email' => 'sometimes|email|unique:users,email',
            'password' => 'sometimes|string|confirmed'
        ];
    }
}
