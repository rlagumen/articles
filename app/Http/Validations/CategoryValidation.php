<?php

namespace App\Http\Validations;

class CategoryValidation
{
    public function save()
    {
        return [
            'name' => 'required|string|unique:categories,name'
        ];
    }

    public function update()
    {
        return [
            'name' => 'sometimes|string|unique:categories,name',
        ];
    }
}
