<?php

namespace App\Http\Controllers;

use App\Http\Repositories\ArticleRepository;
use App\Http\Resources\ArticlesResources;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ArticlesController extends Controller
{
    protected $repository;

    public function __construct(ArticleRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index(Request $request)
    {
        $articles = $this->repository->getArticles($request->all(), ['category', 'media']);

        return ArticlesResources::collection($articles);
    }

    public function show($slug)
    {
        $article = $this->repository->getArticleBySlug($slug, ['category', 'media']);

        return new ArticlesResources($article);
    }
}
