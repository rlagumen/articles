<?php

namespace App\Http\Controllers;

use App\Http\Repositories\CategoryRepository;
use App\Http\Resources\CategoriesResource;

class CategoriesController extends Controller
{
    protected $repository;

    public function __construct(CategoryRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index()
    {
        $categories = $this->repository->getCategories();

        return CategoriesResource::collection($categories);
    }

    public function show($slug)
    {
        $category = $this->repository->getCategoryBySlug($slug, ['creator', 'articles']);

        return new CategoriesResource($category);
    }
}
