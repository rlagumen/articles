<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Repositories\UserRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    protected $repository;

    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
    }

    public function __invoke(Request $request)
    {
        $credentials = $request->only(['email', 'password']);

        $this->validate($request, $this->rules());

        if ($token = Auth::attempt($credentials)) {
            return response()->json([
                'token' => $token,
                'token_type' => 'bearer',
                'expires_in' => (Auth::factory()->getTTL() * 60) * 2 // 2hrs
            ]);
        }

        return response()->json(['message' => 'Invalid Credentials'], 200);
    }

    public function rules()
    {
        return [
            'email' => 'required|email',
            'password' => 'required|string'
        ];
    }
}
