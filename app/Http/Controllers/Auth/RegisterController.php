<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Repositories\UserRepository;
use App\Http\Validations\UserValidation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RegisterController extends Controller
{
    protected $validation;
    protected $repository;

    public function __construct(UserRepository $repository, UserValidation $validation)
    {
        $this->repository = $repository;
        $this->validation = $validation;
    }

    public function index()
    {
        return view('auth.register');
    }

    public function store(Request $request)
    {
        $this->validate($request, $this->validation->save());

        $user = $this->repository->createUser($request->only(['first_name', 'last_name', 'email', 'password']));

        Auth::login($user);

        return redirect()->route('contributor.articles.index');
    }
}
