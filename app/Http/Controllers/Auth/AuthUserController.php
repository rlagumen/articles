<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Resources\UsersResources;
use Illuminate\Support\Facades\Auth;

class AuthUserController extends Controller
{
    public function __invoke()
    {
        $user = Auth::user();

        return Auth::check()
            ? new UsersResources($user)
            : null;
    }
}
