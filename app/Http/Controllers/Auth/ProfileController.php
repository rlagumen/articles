<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Repositories\UserRepository;
use App\Http\Validations\UserValidation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{
    protected $validation;
    protected $repository;

    public function __construct(UserRepository $repository, UserValidation $validation)
    {
        $this->repository = $repository;
        $this->validation = $validation;
    }

    public function index()
    {
        return view('auth.profile', [
            'user' => Auth::user()
        ]);
    }

    public function update(Request $request)
    {
        $this->validate($request, $this->validation->update());

        $this->repository->updateUser(Auth::id(), $request->only(['first_name', 'last_name', 'email']));

        return redirect()->route('profile.index');
    }
}
