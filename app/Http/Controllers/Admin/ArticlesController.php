<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Repositories\ArticleRepository;
use App\Http\Resources\Admin\ArticlesResources;
use App\Http\Validations\ArticleValidation;
use Illuminate\Http\Request;

class ArticlesController extends Controller
{
    protected $repository;
    protected $validations;

    public function __construct(ArticleValidation $validation, ArticleRepository $repository)
    {
        $this->validations = $validation;
        $this->repository = $repository;
    }

    public function index(Request $request)
    {
        $articles = $this->repository->getArticles($request->all(), ['category', 'creator'], 'withTrashed');

        return ArticlesResources::collection($articles);
    }

    public function show($id)
    {
        $this->authorize('articles.show',
            $article = $this->repository->getArticle($id, ['category', 'creator', 'media'])
        );

        return new ArticlesResources($article);
    }

    public function store(Request $request)
    {
        $this->authorize('articles.store');

        $this->validate($request, $this->validations->save());

        $article = $this->repository->createArticle($request->all());

        return new ArticlesResources($article->load('media'));
    }

    public function update($id, Request $request)
    {
        $this->authorize('articles.update');

        $this->validate($request, $this->validations->update());

        $article = $this->repository->updateArticle($id, $request->all());

        return new ArticlesResources($article);
    }

    public function destroy($id)
    {
        $this->authorize('articles.destroy');

        $this->repository->deleteArticle($id);

        return response()->json([], 204);
    }

    public function restore($id)
    {
        $this->authorize('articles.restore');

        $this->repository->restoreArticle($id);

        return response()->json([], 204);
    }
}
