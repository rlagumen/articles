<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Repositories\UserRepository;
use App\Http\Resources\Admin\UsersResources;
use App\Http\Validations\UserValidation;
use Illuminate\Http\Request;

class UsersController extends Controller
{
    protected $repository;
    protected $validations;

    public function __construct(UserValidation $validation, UserRepository $repository)
    {
        $this->validations = $validation;
        $this->repository = $repository;
    }

    public function index()
    {
        $this->authorize('users.index');

        $users = $this->repository->getUsers();

        return UsersResources::collection($users);
    }

    public function show($id)
    {
        $this->authorize('users.show');

        $user = $this->repository->getUser($id);

        return new UsersResources($user);
    }

    public function store(Request $request)
    {
        $this->authorize('users.store');

        $this->validate($request, $this->validations->save());

        $user = $this->repository->createUser($request->all());

        return new UsersResources($user);
    }

    public function update($id, Request $request)
    {
        $this->authorize('users.update');

        $this->validate($request, $this->validations->update());

        $user = $this->repository->updateUser($id, $request->all());

        return new UsersResources($user);
    }

    public function destroy($id)
    {
        $this->authorize('users.destroy');

        $this->repository->deleteUser($id);

        return response()->json([], 204);
    }

    public function restore($id)
    {
        $this->authorize('users.restore');

        $this->repository->restoreUser($id);

        return response()->json([], 204);
    }
}
