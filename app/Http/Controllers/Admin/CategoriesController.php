<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Repositories\CategoryRepository;
use App\Http\Resources\Admin\CategoriesResource;
use App\Http\Validations\CategoryValidation;
use Illuminate\Http\Request;

class CategoriesController extends Controller
{
    protected $repository;
    protected $validations;

    public function __construct(CategoryValidation $validation, CategoryRepository $repository)
    {
        $this->validations = $validation;
        $this->repository = $repository;
    }

    public function index()
    {
        $categories = $this->repository->getCategories(['creator']);

        return CategoriesResource::collection($categories);
    }

    public function show($id)
    {
        $category = $this->repository->getCategory($id, ['creator']);

        return new CategoriesResource($category);
    }

    public function store(Request $request)
    {
        $this->authorize('categories.store');

        $this->validate($request, $this->validations->save());

        $category = $this->repository->createCategory($request->all());

        return new CategoriesResource($category);
    }

    public function update($id, Request $request)
    {
        $this->authorize('categories.update');

        $this->validate($request, $this->validations->update());

        $category = $this->repository->updateCategory($id, $request->all());

        return new CategoriesResource($category);
    }

    public function destroy($id)
    {
        $this->authorize('categories.destroy');

        $this->repository->deleteCategory($id);

        return response()->json([], 204);
    }
}
