<?php

namespace App\Http\Repositories\Traits;

use Illuminate\Support\Str;

trait HasConstraints
{
    /**
     * Parse the constraints into callable query constrains.
     *
     * @param  array  $constraints
     */
    public function parseConstraints($constraints) {
        foreach ($constraints as $constraint) {
            $relation[$constraint] = function ($query) use ($constraints) {
                foreach ($constraints as $constraint) {
                    $constraint = Str::camel($constraint);

                    $query->$constraint();
                }
            };
        }
        return $this->camelKeys($relation)->toArray();
    }

    /**
     * Transform array keys to camel case.
     *
     * @param  Illuminate\Support\Collection|array  $collection
     * @return Illuminate\Support\Collection
     */
    private function camelKeys($collection)
    {
        return collect($collection)->keyBy(function ($item, $key) {
            return Str::camel($key);
        });
    }

}
