<?php

namespace App\Http\Repositories;

use App\Models\User;
use Illuminate\Hashing\BcryptHasher;

class UserRepository
{
    protected $model;

    public function __construct(User $model)
    {
        $this->model = $model;
    }

    public function getUsers()
    {
        $users = $this->model->withTrashed()->get();

        return $users;
    }

    public function getUser($id)
    {
        $user = $this->model->findOrFail($id);

        return $user;
    }

    public function getUserByEmail($email)
    {
        $user = $this->model->where('email', $email)->first();

        return $user;
    }

    public function createUser(array $data)
    {
        $user = $this->model->create([
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'role' => $data['role'] ?? User::contributor(),
            'email' => $data['email'],
            'password' => (new BcryptHasher())->make($data['password'])
        ]);

        return $user;
    }

    public function updateUser($id, array $data)
    {
        /** @var User $user */
        $user = $this->model->findOrFail($id);

        $user->update([
            'first_name' => $data['first_name'] ?? $user->first_name,
            'last_name' => $data['last_name'] ?? $user->last_name,
            'role' => $data['role'] ?? $user->role,
            'email' => $data['email'] ?? $user->email,
            'password' => !empty($data['password']) ? (new BcryptHasher())->make($data['password']) : $user->password
        ]);

        return $user->fresh();
    }

    public function deleteUser($id)
    {
        /** @var User $user */
        $user = $this->model->findOrFail($id);

        $user->delete();

        return $user;
    }

    public function restoreUser($id)
    {
        /** @var User $user */
        $user = $this->model->withTrashed()->find($id);

        $user->restore();

        return $user;
    }
}
