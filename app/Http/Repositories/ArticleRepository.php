<?php

namespace App\Http\Repositories;

use App\Http\Repositories\Traits\HasConstraints;
use App\Models\Article;
use Illuminate\Support\Facades\Auth;

class ArticleRepository
{
    use HasConstraints;

    protected $model;

    public function __construct(Article $model)
    {
        $this->model = $model;
    }

    public function getArticles($filters = [], $with = [], $constraint = null)
    {
        $articles = $this->model->with($with);

        if ($constraint) {
            $articles->$constraint();
        }

        if (Auth::user() && Auth::user()->isContributor()) {
            $articles->where('creator_id', Auth::id());
        }

        if (Auth::guest()) {
            $articles->orderBy('id', 'desc')
                ->where('status', Article::approved());
        }

        return $articles->filter($filters);
    }

    public function getArticle($id, $with = [])
    {
        $article = $this->model->with($with)->findOrFail($id);

        return $article;
    }

    public function getArticleBySlug($slug, $with = [])
    {
        $article = $this->model->with($with)->where('slug', $slug)->first();

        return $article;
    }

    public function createArticle(array $data)
    {
        $article = $this->model->create([
            'category_id' => $data['category'],
            'title' => $data['title'],
            'content' => $data['content'] ?? null,
            'excerpt' => $data['excerpt'] ?? null,
            'status' => $data['status'] ?? 'draft'
        ]);

        if (!empty($data['featured_image'])) {
            $article->addMedia($data['featured_image'], 'featured_image');
        }

        return $article;
    }

    public function updateArticle($id, array $data)
    {
        $article = $this->model->findOrFail($id);

        $article->update([
            'category_id' => $data['category'] ?? $article->category_id,
            'title' => $data['title'] ?? $article->title,
            'content' => $data['content'] ?? $article->content,
            'excerpt' => $data['excerpt'] ?? $article->excerpt,
            'status' => $data['status'] ?? $article->status
        ]);

        if (!empty($data['featured_image'])) {
            $article->addMedia($data['featured_image'], 'featured_image');
        }

        return $article->fresh();
    }

    public function deleteArticle($id)
    {
        $article = $this->model->findOrFail($id);

        $article->delete();

        return $article;
    }

    public function restoreArticle($id)
    {
        $article = $this->model->withTrashed()->find($id);

        $article->restore();

        return $article;
    }
}
