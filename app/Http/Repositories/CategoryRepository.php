<?php

namespace App\Http\Repositories;

use App\Models\Category;

class CategoryRepository
{
    protected $model;

    public function __construct(Category $model)
    {
        $this->model = $model;
    }

    public function getCategories($with = [])
    {
        $categories = $this->model->with($with)->get();

        return $categories;
    }

    public function getCategory($id, $with = [])
    {
        $category = $this->model->with($with)->findOrFail($id);

        return $category;
    }

    public function getCategoryBySlug($slug, $with = [])
    {
        $category = $this->model->with($with)->where('slug', $slug)->first();

        return $category;
    }

    public function createCategory(array $data)
    {
        $category = $this->model->create([
            'name' => $data['name']
        ]);

        return $category;
    }

    public function updateCategory($id, array $data)
    {
        $category = $this->model->findOrFail($id);

        $category->update([
            'name' => $data['name']
        ]);

        return $category->fresh();
    }

    public function deleteCategory($id)
    {
        $category = $this->model->findOrFail($id);

        $category->delete();

        return $category;
    }
}
