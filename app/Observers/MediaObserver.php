<?php

namespace App\Observers;

use App\Models\Media;
use Illuminate\Support\Facades\Auth;

trait MediaObserver
{
    protected static function boot()
    {
        parent::boot();

        static::creating(function (Media $article) {
            $article->user_id = Auth::id();
        });
    }
}
