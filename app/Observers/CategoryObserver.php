<?php

namespace App\Observers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

trait CategoryObserver
{
    protected static function boot()
    {
        parent::boot();

        static::creating(function ($category) {
            $category->creator_id = Auth::id() ?? $category->creator_id;
            $category->slug = Str::slug($category->name);
        });

        static::updating(function ($category) {
            $category->slug = Str::slug($category->name);
        });
    }
}
