<?php

namespace App\Observers;

use App\Models\Article;
use Illuminate\Support\Facades\Auth;

trait ArticleObserver
{
    protected static function boot()
    {
        parent::boot();

        static::creating(function (Article $article) {
            $article->creator_id = Auth::id() ?? $article->creator_id;
            $article->slug = $article->generateSlug($article->title);
        });

        static::updating(function (Article $article) {
            $article->slug = $article->generateSlug($article->title);
        });
    }
}
