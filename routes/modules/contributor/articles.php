<?php

use Illuminate\Support\Facades\Route;

Route::group(['prefix' => 'articles'], function () {
    Route::post('store', ['as' => 'contributor.articles.store', 'uses' => 'Contributor\ArticlesController@store']);
    Route::patch('{id}/update', ['as' => 'contributor.articles.update', 'uses' => 'Contributor\ArticlesController@update']);
    Route::delete('{id}/delete', ['as' => 'contributor.articles.destroy', 'uses' => 'Contributor\ArticlesController@destroy']);
    Route::get('create', ['as' => 'contributor.articles.create', 'uses' => 'Contributor\ArticlesController@create']);
    Route::get('{id}/edit', ['as' => 'contributor.articles.edit', 'uses' => 'Contributor\ArticlesController@edit']);
    Route::get('{id}', ['as' => 'contributor.articles.show', 'uses' => 'Contributor\ArticlesController@show']);
    Route::get('', ['as' => 'contributor.articles.index', 'uses' => 'Contributor\ArticlesController@index']);
});
