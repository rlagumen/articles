<?php

use Illuminate\Support\Facades\Route;

Route::group(['prefix' => 'categories'], function () {
    Route::get('', ['as' => 'contributor.categories.index', 'uses' => 'Contributor\CategoriesController@index']);
});
