<?php

use Illuminate\Support\Facades\Route;

Route::group(['prefix' => 'articles'], function () {
    Route::post('store', ['as' => 'admin.articles.store', 'uses' => 'Admin\ArticlesController@store']);
    Route::patch('{id}/update', ['as' => 'admin.articles.update', 'uses' => 'Admin\ArticlesController@update']);
    Route::patch('{id}/restore', ['as' => 'admin.articles.restore', 'uses' => 'Admin\ArticlesController@restore']);
    Route::delete('{id}/delete', ['as' => 'admin.articles.destroy', 'uses' => 'Admin\ArticlesController@destroy']);
    Route::get('create', ['as' => 'admin.articles.create', 'uses' => 'Admin\ArticlesController@create']);
    Route::get('{id}/edit', ['as' => 'admin.articles.edit', 'uses' => 'Admin\ArticlesController@edit']);
    Route::get('{id}', ['as' => 'admin.articles.show', 'uses' => 'Admin\ArticlesController@show']);
    Route::get('', ['as' => 'admin.articles.index', 'uses' => 'Admin\ArticlesController@index']);
});
