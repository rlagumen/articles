<?php

use Illuminate\Support\Facades\Route;

Route::group(['prefix' => 'categories'], function () {
    Route::post('store', ['as' => 'admin.categories.store', 'uses' => 'Admin\CategoriesController@store']);
    Route::patch('{id}/update', ['as' => 'admin.categories.update', 'uses' => 'Admin\CategoriesController@update']);
    Route::patch('{id}/restore', ['as' => 'admin.categories.restore', 'uses' => 'Admin\CategoriesController@restore']);
    Route::delete('{id}/delete', ['as' => 'admin.categories.destroy', 'uses' => 'Admin\CategoriesController@destroy']);
    Route::get('create', ['as' => 'admin.categories.create', 'uses' => 'Admin\CategoriesController@create']);
    Route::get('{id}/edit', ['as' => 'admin.categories.edit', 'uses' => 'Admin\CategoriesController@edit']);
    Route::get('{id}', ['as' => 'admin.categories.show', 'uses' => 'Admin\CategoriesController@show']);
    Route::get('', ['as' => 'admin.categories.index', 'uses' => 'Admin\CategoriesController@index']);
});
