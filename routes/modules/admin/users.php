<?php

use Illuminate\Support\Facades\Route;

Route::group(['prefix' => 'users'], function () {
    Route::post('store', ['as' => 'admin.users.store', 'uses' => 'Admin\UsersController@store']);
    Route::patch('{id}/update', ['as' => 'admin.users.update', 'uses' => 'Admin\UsersController@update']);
    Route::patch('{id}/restore', ['as' => 'admin.users.restore', 'uses' => 'Admin\UsersController@restore']);
    Route::delete('{id}/delete', ['as' => 'admin.users.destroy', 'uses' => 'Admin\UsersController@destroy']);
    Route::get('create', ['as' => 'admin.users.create', 'uses' => 'Admin\UsersController@create']);
    Route::get('{id}/edit', ['as' => 'admin.users.edit', 'uses' => 'Admin\UsersController@edit']);
    Route::get('{id}', ['as' => 'admin.users.show', 'uses' => 'Admin\UsersController@show']);
    Route::get('', ['as' => 'admin.users.index', 'uses' => 'Admin\UsersController@index']);
});
