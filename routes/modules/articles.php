<?php

use Illuminate\Support\Facades\Route;

Route::group(['prefix' => 'articles'], function () {
    Route::get('{slug}', ['as' => 'articles.show', 'uses' => 'ArticlesController@show']);
    Route::get('', ['as' => 'articles.index', 'uses' => 'ArticlesController@index']);
});
