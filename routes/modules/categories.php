<?php

use Illuminate\Support\Facades\Route;

Route::group(['prefix' => 'categories'], function () {
    Route::get('{slug}', ['as' => 'categories.show', 'uses' => 'CategoriesController@show']);
    Route::get('', ['as' => 'categories.index', 'uses' => 'CategoriesController@index']);
});
