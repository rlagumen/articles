<?php

/** @var Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

use Illuminate\Support\Facades\Route;
use Laravel\Lumen\Routing\Router;

Route::get('register', ['uses' => 'Auth\RegisterController@index']);
Route::post('register', ['uses' => 'Auth\RegisterController@store']);

Route::group(['prefix' => 'spa'], function () {
    Route::post('login', ['uses' => 'Auth\LoginController']);

    require __DIR__.'/modules/categories.php';
    require __DIR__.'/modules/articles.php';

    Route::group(['middleware' => 'auth'], function () {
        Route::get('profile', ['as' => 'profile.index', 'uses' => 'Auth\ProfileController@index']);
        Route::patch('profile/update', ['as' => 'profile.update', 'uses' => 'Auth\ProfileController@update']);

        Route::group(['prefix' => 'admin'], function () {
            require __DIR__.'/modules/admin/categories.php';
            require __DIR__.'/modules/admin/articles.php';
            require __DIR__.'/modules/admin/users.php';
        });

        Route::group(['prefix' => 'contributor'], function () {
            require __DIR__.'/modules/contributor/articles.php';
            require __DIR__.'/modules/contributor/categories.php';
        });

        Route::get('auth/user', ['uses' => 'Auth\AuthUserController']);
    });
});

$router->get('/{route:.*}/', function ()  {
    return view('spa');
});
