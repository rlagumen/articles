const mix = require('laravel-mix')
const path = require('path')

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

const config = {
    resolve: {
        alias: {
            // '@directives': path.resolve(__dirname, 'resources/js/directives'),
            '@components': path.resolve(__dirname, 'resources/js/components'),
            '@router': path.resolve(__dirname, 'resources/js/router'),
            '@mixins': path.resolve(__dirname, 'resources/js/mixins'),
            // '@const': path.resolve(__dirname, 'resources/js/const'),
            '@store': path.resolve(__dirname, 'resources/js/store'),
            '@utils': path.resolve(__dirname, 'resources/js/utils'),
            '@pages': path.resolve(__dirname, 'resources/js/pages'),
            '@api': path.resolve(__dirname, 'resources/js/api'),
            '@src': path.resolve(__dirname, 'resources/js')
        }
    },
    output: {
        chunkFilename: 'js/[name].[contenthash].js'
    }
}

mix
    .setPublicPath('public')
    .js('resources/js/app.js', 'public/js')
    .sass('resources/sass/app.scss', 'public/css')
    .copy('node_modules/bootstrap/dist/css/bootstrap.css',
        'public/css/bootstrap.css')
    .copy('node_modules/bootstrap/dist/js/bootstrap.min.js',
        'public/js/bootstrap.min.js')
    .webpackConfig(config)
    .sourceMaps()
    .version()
