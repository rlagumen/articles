<!DOCTYPE html>
<html lang="en">
<head>
    <title>Optimy Articles</title>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="{{ url('/css/app.css') }}" rel="stylesheet">
</head>
<body>
<div id="app"></div>
<script src="{{ url('/js/app.js') }}"></script>
</body>
</html>


