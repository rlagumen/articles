<html lang="en"><head>
    <meta charset="utf-8">
    <title>Optimy Articles</title>
    <link href="{{ url('/css/auth.css') }}" rel="stylesheet">
</head>
<body class="text-center">

<form action="/login" method="POST" class="form-signin">
    <h1 class="h3 mb-3 font-weight-normal">Log in</h1>
    <label for="inputEmail" class="sr-only">Email address</label>
    <input type="email" name="email" id="inputEmail" class="form-control" placeholder="Email address" required="" autofocus="">
    <label for="inputPassword" class="sr-only">Password</label>
    <input type="password" name="password" id="inputPassword" class="form-control" placeholder="Password" required="">
    <div class="checkbox mb-3">
        <label>
            <input type="checkbox" value="remember-me"> Remember me
        </label>
    </div>
    <button class="btn btn-lg btn-primary btn-block mb-3" type="submit">Sign in</button>
    <a href="/">Go back</a>
</form>
</body>
</html>
