<!DOCTYPE html>
<html lang="en">
<head>
    <title>Optimy Articles</title>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    @stack('styles')
</head>
<body>
<div id="app">
    @yield('content')
</div>

@stack('scripts')
</body>
</html>
