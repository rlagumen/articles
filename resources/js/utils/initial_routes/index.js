import store from "@store"
import redirector from './default_page_redirector'

export default {
    defaultPageRedirect() {
        return redirector.getDefaultPage(store.getters['user/role'])
    }
}
