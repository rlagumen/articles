const defaultPage = () => ({
    contributor: '/contributor',
    admin: '/admin'
})

export default {
    getDefaultPage(role) {
        try {
            defaultPage()[role]
        } catch (e) {
            return undefined
        }
    }
}
