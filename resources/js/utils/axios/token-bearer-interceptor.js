import axios from "axios"
import store from '@store'

axios.interceptors.request.use((request) => {
    if (request.data && request.headers['Content-Type'] === 'application/x-www-form-urlencoded') {
        request.data = qs.stringify(request.data)
    }
    request.headers['Authorization'] = store.getters['user/token']
    return request
})
