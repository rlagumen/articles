import axios from 'axios'
import NProgress from "nprogress";

let requestCount = 0

const progressStart = () => {
    if(requestCount === 0) NProgress.start()
    ++requestCount
}

const progressEnd = () => {
    if(--requestCount > 0) {
        NProgress.inc()
    } else {
        NProgress.done()
    }
}

axios.interceptors.request.use(
    (config) => {
        progressStart()
        return config
    },
    (error) => {
        progressEnd()
        return Promise.reject(error)
    }
)

axios.interceptors.response.use(
    (response) => {
        progressEnd()
        return response
    },
    (error) => {
        progressEnd()
        return Promise.reject(error)
    }
)
