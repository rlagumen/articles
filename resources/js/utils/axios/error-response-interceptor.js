import axios from "axios"
import router from "@router"

axios.interceptors.response.use(
    (response) => response,
    (error) => {
        const {response} = error

        console.log(error)

        const errors = [403, 404, 500, 419, 401]
        if(_.includes(errors, response.status)) {
            if([404, 403, 401, 419].includes(response.status)) {
                router.push({
                    path: `/error/${response.status}`
                })
            } else {
                router.push({
                    path: `/error/${response.status}`,
                    query: {
                        reference: `${router.currentRoute.fullPath}`
                    }
                })
            }
        }

        return Promise.reject(error)
    }
)
