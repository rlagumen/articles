import cookie from 'vue-cookies'
import { GET_CURRENT_USER, SET_CURRENT_USER, SET_JWT_TOKEN } from '@store/action_types'
import store from '@store'
import * as apiAuth from '@api/auth'

const USER_DETAILS = 'current_user'
const TOKEN = 'token'

export const removeUserSession = () => {
    cookie.remove(USER_DETAILS)
    cookie.remove(TOKEN)
}

export const setUserSession = (data) => {
    let cached = cookie.get(USER_DETAILS)
    let cachedToken = cookie.get(TOKEN)
    if (!cached) cached = {}

    cookie.set(
        USER_DETAILS,
        {
            ...cached,
            ...data.user
        }, `${data.token.expired_in}s`
    )

    cookie.set(
        TOKEN,
        {
            ...cachedToken,
            ...data.token
        }, `${data.token.expired_in}s`
    )
}

export default {
    async login () {
        if (!cookie.get(TOKEN)) {
            let data = await store.dispatch(`user/${GET_CURRENT_USER}`);
            if (data) {
                setUserSession(data)
            }
        }
        store.commit(`user/${SET_CURRENT_USER}`, cookie.get(USER_DETAILS))
        store.commit(`user/${SET_JWT_TOKEN}`, cookie.get(TOKEN))
    },

    logout () {
        removeUserSession()
        return new Promise((resolve, reject) => {
            apiAuth
                .signOut()
                .then(async () => {
                    window.location.href = '/'
                    return resolve('logout success')
                })
                .catch(() => reject('logout failed'))
        })
    },

    removeUserSession
}
