import axios from 'axios'

export function login (params) {
    return axios.post('/spa/login', params)
}

export function getUser (params) {
    return axios.get('/spa/auth/user', params)
}

export function signOut (params) {
    return axios.get('/logout', params)
}

export function updateUser (params, config) {
    return axios.get('/auth/user/update', params, config)
}

export function changePassword (params) {
    return axios.get('/auth/user/change-password', params)
}

export default {
    getUser,
    signOut,
    updateUser,
    changePassword
}
