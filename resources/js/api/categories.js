import axios from 'axios'

export function index(params) {
    return axios.get('/spa/categories', params)
}

export function show(slug, params) {
    return axios.get(`/spa/categories/${slug}`, params)
}

export default {
    index,
    show
}
