import axios from 'axios'

export function index(params) {
    return axios.get('/spa/admin/articles', params)
}

export function show(id, params) {
    return axios.get(`/spa/admin/articles/${id}`, params)
}

export function store(params) {
    return axios.post(`/spa/admin/articles/store`, params)
}

export function update(id, params) {
    return axios.patch(`/spa/admin/articles/${id}/update`, params)
}

export function destroy(id, params) {
    return axios.delete(`/spa/admin/articles/${id}/delete`, params)
}

export function restore(id, params) {
    return axios.patch(`/spa/admin/articles/${id}/restore`, params)
}

export default {
    index,
    show,
    store,
    update,
    destroy,
    restore
}
