import axios from 'axios'

export function index(params) {
    return axios.get('/spa/admin/categories', params)
}

export function show(slug, params) {
    return axios.get(`/spa/admin/categories/${slug}`, params)
}

export function store(slug, params) {
    return axios.post(`/spa/admin/categories/${slug}`, params)
}

export function update(slug, params) {
    return axios.patch(`/spa/admin/categories/${slug}`, params)
}

export function destroy(slug, params) {
    return axios.delete(`/spa/admin/categories/${slug}`, params)
}

export default {
    index,
    show,
    store,
    update,
    destroy
}
