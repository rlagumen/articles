import axios from 'axios'

export function index(params) {
    return axios.get('/spa/articles', params)
}

export function show(slug, params) {
    return axios.get(`/spa/articles/${slug}`, params)
}

export default {
    index,
    show
}
