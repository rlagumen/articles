export default {
    data () {
        return {
            errorBag: null
        }
    },

    methods: {
        getErrors () {
            try {
                return this.errorBag.errors
            } catch (e) {
                return ''
            }
        },

        getFormError (key, errorBag) {
            try {
                if (errorBag) return this[errorBag].errors[key][0]
                return this.errorBag[key][0]
            } catch (e) {
                return ''
            }
        },

        setFormErrors (errors, key) {
            if (key) {
                this[key] = errors
                return
            }
            this.errorBag = errors
        }
    }
}
