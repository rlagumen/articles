import moment from 'moment-timezone'

export default {
    filters: {
        shortDate (date) {
            return moment.utc(date).format('MMM d')
        },
        simpleDate (date) {
            return moment.utc(date).format('MMMM DD, YYYY')
        },
        simpleDateTime (date) {
            return moment
                .utc(date)
                .format('MM-DD-YYYY hh:mm A')
        }
    }
}
