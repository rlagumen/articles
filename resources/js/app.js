import 'core-js'
import '@babel/polyfill'
import Vue from 'vue'
import store from '@store'
import router from './router'
import App from './pages/App'
import './components/_globals'
// import './directives'
import '@utils/axios'

require('./bootstrap')

window.Vue = Vue

new Vue({
    el: '#app',
    components: { App },
    store,
    router,
    template: '<App/>'
})
