import RouteWrapper from '@components/RouteWrapper'

export const getRoute = () => [
    {
        path: 'articles',
        component: RouteWrapper,
        children: [
            {
                path: '/',
                component: () => import('@pages/admin/articles/List')
            },
            {
                path: 'create',
                component: () => import('@pages/admin/articles/Form')
            },
            {
                path: ':id/edit',
                component: () => import('@pages/admin/articles/Form')
            }
        ]
    }
]

export default { getRoute }
