import { GET_CURRENT_USER } from '@store/action_types'
import cookie from 'vue-cookies'
import store from '../store'
import auth from '@utils/auth'

const USER_DETAILS = 'current_user'

/**
 * Check for parent meta first. If not found
 * proceed with the original meta
 *
 * @param {Object} to
 */
const getMeta = (to) => {
    if (to.meta.permission) {
        return to.meta
    }

    // Check if matched is greater than 1, meaning there might be parent
    if (to.matched.length > 1) {
        const { parent } = to.matched[to.matched.length - 1]

        if (!!parent && !!parent.meta && parent.meta.permission) {
            return parent.meta
        }
    }

    return to.meta
}

const getUser = async () => {
    let user = cookie.get(USER_DETAILS)
    if (!user) {
        return null
    }
    return user
}

const Gate = async (to, from, next) => {
    const meta = getMeta(to)

    if (!!meta && !!meta.permission) {
        const user = await getUser()
        const { permission } = meta
        const role = (!!user.role) || ''
        const { type } = user

        // check if array and if includes this role
        if (
            Array.isArray(permission) &&
            (permission.includes(role) || permission.includes(type))
        ) {
            return next()
        }

        // check if role is same
        if ([role, type].includes(permission)) {
            return next()
        }

        return next('/error/403')
    }

    return next()
}

export const AuthorizedAdminAccess = async (to, from, next) => {
    const user = await getUser()

    if (!user || user.role !== 'admin') return next('/error/403')

    return next()
}

export const AuthorizedContributorAccess = async (to, from, next) => {
    const user = await getUser()

    if (!user || user.role !== 'contributor') {
        return next('/error/403')
    }

    return next()
}


export default Gate
