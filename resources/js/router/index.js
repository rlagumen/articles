import Vue from 'vue'
import VueRouter from 'vue-router'
import NProgress from 'nprogress'
import RouteWrapper from "@components/RouteWrapper"
import authenticate from "./auth"
import * as AdminModules from './modules/admin'
import {
    AuthorizedAdminAccess
} from "./AuthorizationGate";

Vue.use(VueRouter)

const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/admin',
            component: () => import('@pages/admin/Dashboard'),
            beforeEnter: authenticate,
            children: [
                {
                    path: '/',
                    beforeEnter: AuthorizedAdminAccess,
                    component: RouteWrapper,
                    children: [
                        ...AdminModules.Articles.getRoute()
                    ]
                }
            ]
        },
        {
            path: '/login',
            component: () => import('@pages/auth/Login'),
            beforeEnter: authenticate
        },
        {
            path: '/categories/:id',
            component: () => import('@pages/categories/Detail'),
            beforeEnter: authenticate
        },
        {
            path: '/articles',
            component: RouteWrapper,
            beforeEnter: authenticate,
            children: [
                {
                    path: '/',
                    component: () => import('@pages/articles/List')
                },
                {
                    path: ':id',
                    component: () => import('@pages/articles/Detail')
                }
            ]
        },
        {
            path: '',
            component: () => import('@pages/Home'),
            beforeEnter: authenticate
        }
    ]
})

router.beforeResolve((to, from, next) => {
    NProgress.start()
    next()
});

router.afterEach(() => NProgress.done())

export default router
