import auth from "@utils/auth"
import initialRoutes from "@utils/initial_routes"

export default async (to, from, next) => {
    await auth.login()
    // const page = initialRoutes.defaultPageRedirect()

    // if (page && to.path === '/') return next({path: page})
    return next()
}
