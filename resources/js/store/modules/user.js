import {GET_CURRENT_USER, SET_CURRENT_USER, SET_JWT_TOKEN} from "@store/action_types";
import * as apiAuth from "@api/auth"
import {reject} from "lodash/collection";

const state = {
    information: null
}

const getters = {
    role () {
        if (!state.information) return ''
        return state.information.user && state.information.user.role
    },

    details () {
        if (!state.information) return ''
        return state.information.user
    },

    token() {
        if (!state.information || !state.information.auth) return ''
        return `${state.information.auth.token_type} ${state.information.auth.token}`
    },

    auth() {
        return state.information && state.information.auth
    }
}

const mutations = {
    [SET_CURRENT_USER] (state, user) {
        state.information = {
            ...state.information,
            user
        }
    },
    [SET_JWT_TOKEN] (state, token) {
        state.information = {
            ...state.information,
            auth: token
        }
    }
}

const actions = {
    [GET_CURRENT_USER] ({commit}) {
        if (!state.information || !state.information.auth) return null
        return apiAuth.getUser({
            headers: {
                Authorization: `${state.information.auth.token_type} ${state.information.auth.token}`
            }
        }).then(({ data }) => {
            commit(SET_CURRENT_USER, data.data)
            return {
                user: data.data,
                token: state.information.auth
            }
        })
    }
}

export default {
    namespaced: true,
    state,
    getters,
    mutations,
    actions
}
