<?php

namespace Database\Factories;

use App\Models\Article;
use App\Models\Category;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Hashing\BcryptHasher;
use Illuminate\Support\Arr;

class ArticleFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Article::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $category = Category::factory()->create();

        return [
            'category_id' => $category->id,
            'title' => $this->faker->realText(25),
            'content' => $this->faker->sentence(500),
            'excerpt' => $this->faker->realText(150),
        ];
    }
}
