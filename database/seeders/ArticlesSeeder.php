<?php

namespace Database\Seeders;

use App\Models\Article;
use App\Models\Category;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Arr;

class ArticlesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::where('email', 'ralph.lagumen@optimy.com')->first();

        Article::factory()->count(4)->state([
            'creator_id' => $user->id,
            'status' => Article::approved()
        ])->create();

        Article::factory()->count(4)->state([
            'creator_id' => $user->id,
            'status' => Article::approved()
        ])->create();

        Article::factory()->count(4)->state([
            'creator_id' => $user->id,
            'status' => Article::approved()
        ])->create();
    }
}
