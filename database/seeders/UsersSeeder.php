<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Hashing\BcryptHasher;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::factory()->state([
            'role' => User::admin(),
            'email' => 'ralph.lagumen@optimy.com',
            'password' => (new BcryptHasher())->make('secret')
        ])->create();
    }
}
